const jsonServer = require('json-server');
const express = require("express");
const server = jsonServer.create();
const router = jsonServer.router('./db.json');
const middlewares = jsonServer.defaults();

function simpleAuth(req, res, next) {
    const auth = req.headers.authorization;

    if (auth && auth === 'Bearer 123') {

        // continue doing json-server magic
        next();

    } else {
        // it is not recommended in REST APIs to throw errors,
        // instead, we send 401 response with whatever erros
        // we want to expose to the client
        res.status(401).send({error: 'unauthorized'});
    }
}

// // this method overwrites the original json-server's way of response
// // with your custom logic, here we will add the user to the response
// router.render = function (req, res) {
//
//     // manually wrap any response send by json-server into an object
//     // this is something like `res.send()`, but it is cleaner and meaningful
//     // as we're sending json object not normal text/response
//     res.json({
//         body: res.locals.data // the original server-json response is stored in `req.locals.data`
//     })
// };

// start setting up json-server middlewares
server.use(middlewares);

// before proceeding with any request, run `simpleAuth` function
// which should check for basic authentication header .. etc
server.use(simpleAuth);

server.use(jsonServer.bodyParser);
server.use(jsonServer.rewriter({
    '/projects/:id/launcher_stages': '/projects/:id/',
    '/projects/:id/launcher_builds': '/projects/:id/',
    '/projects/:id/launcher_releases/latest': '/projects/:id/',
    '/projects/:id/launcher_builds?limit=1&after=35': '/pagination',
}));

// continue doing json-server magic
server.use(router);


// start listening to port 3000
server.listen(3000, function () {
    console.log('JSON Server is running on port 3000');
});