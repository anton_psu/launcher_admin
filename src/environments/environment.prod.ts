export const environment = {
    production: true,
    ftp: {
        server: 'us-static.loc',
        login: '',
        pass: ''
    },
    api: {
        ftp: '/api/ftp',
        proj: '/api/proj'
    },
    alert: {
        action: 'Close',
        error: {
            duration: 8000,
            panelClass: 'error-message'
        },
        info: {
            duration: 5000,
        }
    }
};
