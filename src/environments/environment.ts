// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    ftp: {
        server: 'localhost',
        login: 'test',
        pass: 'test',
    },
    api: {
        ftp: 'https://localhost:4000/api/ftp',
        proj: 'https://localhost:4000/api/proj'
    },
    alert: {
        action: 'Close',
        error: {
            duration: 8000,
            panelClass: 'error-message'
        },
        info: {
            duration: 5000,
        }
    }
};
