import {Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";

@Component({
    selector: 'api',
    templateUrl: './api.component.html',
    styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {
    public hasApi = false;
    public hasProject = false;
    constructor(private data: DataService) {
    }

    ngOnInit() {
        this.data.currentApi.subscribe(next => this.hasApi = next !== '');
        this.data.currentProject.subscribe(next => this.hasProject = next.key !== '');
    }
}