import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpErrorResponse, HttpRequest} from "@angular/common/http";

import {getHeaders} from "../../utils/session";
import {environment} from "../../../environments/environment";
import {DataService} from "../../data.service";
import {MatSnackBar} from "@angular/material";

interface ProjectInterface {
    name: string,
    id: string,
    key: string
}

@Component({
    selector: 'project',
    styleUrls: ['./project.component.css'],
    templateUrl: './project.component.html'
})
export class ProjectComponent implements OnInit {
    ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: [null, [Validators.required]],
            id: [null, [Validators.required]],
            key: [null, [Validators.required]],
        });
        this.load();
        if (!this.all) {
            this.save();
            this.all = [];
        }
        this.data.currentApi.subscribe(next => this.api = next);
    }

    api: string;
    isSelecting = true;
    state = 'add';
    form: FormGroup;
    current: number = -1;
    all: ProjectInterface[] = [];

    constructor(private formBuilder: FormBuilder,
                private  http: HttpClient,
                private data: DataService,
                private snackBar: MatSnackBar) {
    }

    saveApi(newApi: string) {
        this.data.changeApi(newApi);
    }

    addProject() {
        this.isSelecting = false;
        this.state = 'Add';
    }

    patchValue(value: { [key: string]: any }): void {
        Object.keys(value).forEach(name => {
            if (this.form.controls[name]) {
                this.form.controls[name].patchValue(value[name]);
            }
        });
    }

    editProject() {
        this.isSelecting = false;
        this.state = 'Save';
        this.patchValue(this.all[this.current]);
    }

    deleteProject() {
        this.all.splice(this.current, 1);
        this.current = this.all.length > 0 ? 0 : -1;
        this.save();
    }

    changeCurrent(newVal) {
        this.current = newVal;
        this.data.changeProject(this.all[newVal]);
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
    }

    save() {
        this.patchValue({id: "", key: "", name: ""});
        let req = new HttpRequest('POST', `${environment.api.proj}/project`, this.all, {
            responseType: 'text',
            headers: getHeaders()
        });

        this.http.request(req).subscribe(
            (data: any) => {
                if (data.body) this.all = JSON.parse(data.body);
                this.data.changeProject(
                    this.all.length === 0
                        ? {id: "", key: "", name: ""}
                        : this.all[this.current]
                );
            },
            (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
        );
    }

    load() {
        this.http.get(`${environment.api.proj}/project`, {
            responseType: 'text',
            headers: getHeaders()
        }).subscribe(
            (data: any) => {
                this.all = JSON.parse(data);
                this.current = 0;
                this.data.changeProject(this.all[0]);
            },
            (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
        );
    }

    submitProject(values) {
        this.isSelecting = true;
        if (this.state === 'Add') {
            this.all.push(values);
            this.current = this.all.length - 1;
        }
        else {
            this.all = this.all.map((item, index) => index === this.current ? values : item);
        }
        this.save();
    }

    cancelAdding() {
        this.isSelecting = true;
        this.patchValue({id: "", key: "", name: ""});
    }
}