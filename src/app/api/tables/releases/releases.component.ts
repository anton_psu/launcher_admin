import {Component, Input, OnInit} from '@angular/core';
import {getAuthHeader} from "../../../utils/auth";
import {MatDialog, MatSnackBar} from "@angular/material";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataService} from "../../../data.service";
import {Project} from "../../../types/project";
import {ConfirmDialogComponent} from "../../../confirm-dialog/confirm-dialog.component";
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-releases',
    templateUrl: './releases.component.html',
    styleUrls: ['./../tables.component.css']
})
export class ReleasesComponent implements OnInit {
    baseUrl: string;
    project: Project;
    rows: any[] = [];
    builds: Build[] = [];
    stages: Stage[] = [];
    nextPage: string = '';
    form: FormGroup;
    status: string;
    arch: string;
    isLoading = false;
    stage: string = null;

    @Input()
    set setBaseUrl(value: string) {
        this.baseUrl = value;
        this.update();
    }

    private appendBuilds() {
        if (this.project && this.project.key && this.nextPage) {
            this.http
                .get(this.nextPage, {
                    responseType: 'text',
                    headers: getAuthHeader(this.project.key)
                })
                .subscribe(
                    (data: any) => {
                        const parsedData = JSON.parse(data);
                        this.builds = [
                            ...this.builds,
                            ...parsedData.launcher_builds
                        ];
                        this.nextPage = parsedData.next_url ? parsedData.next_url : '';
                        if (this.nextPage)
                            this.appendBuilds();
                    },
                    (err: HttpErrorResponse) => this.handleError(err.message)
                );
        }
    }

    getBuilds() {
        return new Promise((resolve, reject) => {
            if (this.project && this.project.key && this.baseUrl) {
                this.builds = [];
                this.http
                    .get(`${this.baseUrl}/launcher_builds`, {
                        responseType: 'text',
                        headers: getAuthHeader(this.project.key)
                    })
                    .subscribe(
                        (data: any) => {
                            const parsedData = JSON.parse(data);
                            this.builds = parsedData.launcher_builds;
                            this.nextPage = parsedData.next_url ? parsedData.next_url : '';
                            if (this.nextPage)
                                this.appendBuilds();
                            resolve(this.builds);
                        },
                        (err: HttpErrorResponse) => {
                            this.handleError(err.message);
                            reject(err);
                        }
                    );
            }
            else {
                reject(Error('No project or base url'));
            }
        });
    }

    getStages() {
        return new Promise((resolve, reject) => {
            if (this.project && this.project.key && this.baseUrl) {
                this.stages = [];
                this.http
                    .get(`${this.baseUrl}/launcher_stages`, {
                        responseType: 'text',
                        headers: getAuthHeader(this.project.key)
                    })
                    .subscribe(
                        (data: any) => {
                            this.stages = [
                                {
                                    name: 'production',
                                    id: -1,
                                    secret_key: ''
                                },
                                ...JSON.parse(data).launcher_stages
                            ];
                            resolve(this.stages);
                        },
                        (err: HttpErrorResponse) => {
                            this.handleError(err.error && JSON.stringify(err.error.error) || err.message);
                            reject(err);
                        }
                    );
            }
            else {
                reject(Error('No project or base url'));
            }
        });

    }

    update() {
        this.isLoading = true;
        Promise.all([this.getStages(), this.getBuilds()]).then(values => {
            if (this.project && this.project.key && this.baseUrl) {
                this.rows = [];
                const params = {
                    architecture: this.arch
                };
                if (this.stage !== null && this.stage !== 'production') {
                    const stage = this.stages.find(item => item.name === this.stage);
                    params['stage'] = this.stage;
                    params['secret_key'] = stage.secret_key;
                }
                this.http
                    .get(`${this.baseUrl}/launcher_releases/latest`, {
                        responseType: 'text',
                        headers: getAuthHeader(this.project.key),
                        params
                    })
                    .subscribe(
                        (data: any) => {
                            const release = JSON.parse(data);
                            this.rows = [{
                                id: release.id,
                                stage: release.stage,
                                build_id: release.build.id,
                                build_version: release.build.version
                            }];
                            this.isLoading = false;
                        },
                        (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error) || err.message)
                    );
            }
        })
            .catch(err => {});
    }

    paramsChanged() {
        this.update();
    }

    submitForm(values) {
        this.submitAdd(values);
        this.cancel();
    }

    submitAdd(values) {
        const clearValues = {
            ...values
        };
        if (clearValues.stage === 'production') delete clearValues.stage;
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            ...getAuthHeader(this.project.key)
        });
        this.http
            .post(`${this.baseUrl}/launcher_releases`, JSON.stringify(clearValues), {
                headers
            })
            .subscribe(
                (data: any) => {
                    this.update();
                },
                (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
            )
    }

    cancel() {
        this.form.reset();
        this.status = 'Add';
    }

    delete(rowIndex: number) {
        const item = this.rows[rowIndex];
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                confirmMessage: `Do you really want to delete release?`
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.isLoading = true;
                this.http
                    .delete(`${this.baseUrl}/launcher_releases/${item.id}`, {
                        headers: getAuthHeader(this.project.key)
                    })
                    .subscribe(
                        (data: any) => {
                            this.update();
                            this.isLoading = false;
                        },
                        (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
                    );
            }
        });
    }

    constructor(private http: HttpClient,
                private formBuilder: FormBuilder,
                private data: DataService,
                private dialog: MatDialog,
                private snackBar: MatSnackBar) {
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
        this.isLoading = false;
    }

    ngOnInit() {
        this.arch = 'x86';
        this.stage = 'production';
        this.form = this.formBuilder.group({
            build_id: [null, [Validators.required]],
            stage: [null, [Validators.required]],
        });
        this.data.currentProject.subscribe(project => {
            this.project = project;
            this.update();
        });
        this.status = 'Add';
    }
}
