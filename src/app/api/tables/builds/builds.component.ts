import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {getAuthHeader} from "../../../utils/auth";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataService} from "../../../data.service";
import {Project} from "../../../types/project";
import {MatSnackBar} from "@angular/material";
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-builds',
    templateUrl: './builds.component.html',
    styleUrls: ['./../tables.component.css']
})
export class BuildsComponent implements OnInit {
    baseUrl: string;
    project: Project;
    rows: Build[] = [];
    form: FormGroup;
    status: string;
    isLoading: boolean;
    nextPage: string;
    lastBuild: number;

    @Input()
    set setBaseUrl(value: string) {
        this.baseUrl = value;
        this.update();
    }

    isEditing(): boolean {
        return this.status === 'Edit';
    }

    canLoadMore(): boolean {
        return !!this.nextPage;
    }

    loadPage() {
        if (this.project && this.project.key && this.nextPage) {
            this.isLoading = true;

            this.http
                .get(this.nextPage, {
                    responseType: 'text',
                    headers: getAuthHeader(this.project.key)
                })
                .subscribe(
                    (data: any) => {
                        const parsedData = JSON.parse(data);
                        this.rows = [
                            ...this.rows,
                            ...parsedData.launcher_builds
                        ];
                        this.updateBuildNumber();
                        this.nextPage = parsedData.next_url ? parsedData.next_url : '';
                        this.isLoading = false;
                    },
                    (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
                );
        }
    }

    update() {
        if (this.project && this.project.key && this.baseUrl) {
            this.rows = [];
            this.isLoading = true;
            this.http
                .get(`${this.baseUrl}/launcher_builds`, {
                    responseType: 'text',
                    headers: getAuthHeader(this.project.key)
                })
                .subscribe(
                    (data: any) => {
                        const parsedData = JSON.parse(data);
                        this.rows = parsedData.launcher_builds;
                        this.updateBuildNumber();
                        this.nextPage = parsedData.next_url ? parsedData.next_url : '';
                        this.isLoading = false;
                    },
                    (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
                );
        }
    }

    edit(rowIndex: number) {
        this.status = 'Edit';
        this.patchValue(this.rows[rowIndex]);
    }

    submitForm(values) {
        this.isLoading = true;
        if (this.status === 'Edit') {
            this.submitPatch(values);
        } else if (this.status === 'Add') {
            this.submitAdd(values);
        }
        this.cancel();
    }

    submitPatch(values: Build) {
        const clearValues = {
            file_name: values.file_name,
            installer_file_name: values.installer_file_name,
        };
        this.http
            .patch(`${this.baseUrl}/launcher_builds/${values.id}`, clearValues, {
                headers: getAuthHeader(this.project.key)
            })
            .subscribe(
                (data: any) => {
                    this.update();
                    this.isLoading = false;
                },
                (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
            )
    }

    submitAdd(values) {
        const clearValues = {
            ...values
        };
        delete clearValues.id;
        this.http
            .post(`${this.baseUrl}/launcher_builds`, clearValues, {
                headers: getAuthHeader(this.project.key)
            })
            .subscribe(
                (data: any) => {
                    this.update();
                    this.isLoading = false;
                },
                (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
            )
    }

    cancel() {
        this.form.reset();
        this.status = 'Add';
    }

    updateBuildNumber() {
        this.rows.forEach((value: Build) => {
            if (!this.lastBuild || value.build_number > this.lastBuild)
                this.lastBuild = value.build_number;
        });
        this.form.controls.build_number.patchValue(this.lastBuild + 1);
    }

    patchValue(value: { [key: string]: any }): void {
        Object.keys(value).forEach(name => {
            if (this.form.controls[name]) {
                this.form.controls[name].patchValue(value[name]);
            }
        });
    }

    constructor(private http: HttpClient,
                private formBuilder: FormBuilder,
                private data: DataService,
                private el: ElementRef,
                private snackBar: MatSnackBar) {
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
        this.isLoading = false;
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            id: [null],
            version: [null, [Validators.required]],
            build_number: [null, [Validators.required]],
            architecture: ['x86', [Validators.required]],
            file_name: [null, [Validators.required]],
            installer_file_name: [null],
        });
        this.data.currentProject.subscribe(project => {
            this.project = project;
            this.update();
        });
        this.status = 'Add';
    }
}
