import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";

import {getAuthHeader} from "../../../utils/auth";
import {DataService} from "../../../data.service";
import {Project} from "../../../types/project";
import {MatSnackBar} from "@angular/material";
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-stages',
    templateUrl: './stages.component.html',
    styleUrls: ['./../tables.component.css']
})
export class StagesComponent implements OnInit {
    baseUrl: string;
    project: Project;
    rows: Stage[] = [];
    form: FormGroup;
    isLoading = false;
    status: string;

    @Input()
    set setBaseUrl(value: string) {
        this.baseUrl = value;
        this.update();
    }

    isEditing(): boolean {
        return this.status === 'Edit';
    }

    update() {
        if (this.project && this.project.key && this.baseUrl) {
            this.rows = [];
            this.isLoading = true;
            this.http
                .get(`${this.baseUrl}/launcher_stages`, {
                    responseType: 'text',
                    headers: getAuthHeader(this.project.key)
                })
                .subscribe(
                    (data: any) => {
                        this.rows = JSON.parse(data).launcher_stages;
                        this.isLoading = false;
                    },
                    (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
                );
        }
    }

    edit(rowIndex: number) {
        this.status = 'Edit';
        this.patchValue(this.rows[rowIndex]);
    }

    submitForm(values) {
        this.isLoading = true;
        if (this.status === 'Edit') {
            this.submitPatch(values);
        } else if (this.status === 'Add') {
            this.submitAdd(values);
        }
        this.cancel();
    }

    submitPatch(values: Stage) {
        const clearValues = {
            ...values
        };
        delete clearValues.id;
        delete clearValues.name;
        this.http
            .patch(`${this.baseUrl}/launcher_stages/${values.id}`, clearValues, {
                headers: getAuthHeader(this.project.key)
            })
            .subscribe(
                (data: any) => {
                    this.update();
                    this.isLoading = false;
                },
                (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
            )
    }

    submitAdd(values) {
        const clearValues = {
            ...values
        };
        delete clearValues.id;
        this.http
            .post(`${this.baseUrl}/launcher_stages`, clearValues, {
                headers: getAuthHeader(this.project.key)
            })
            .subscribe(
                (data: any) => {
                    this.update();
                    this.isLoading = false;
                },
                (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
            );
    }

    cancel() {
        this.form.reset();
        this.status = 'Add';
    }

    patchValue(value: { [key: string]: any }): void {
        Object.keys(value).forEach(name => {
            if (this.form.controls[name]) {
                this.form.controls[name].patchValue(value[name]);
            }
        });
    }

    constructor(private http: HttpClient,
                private formBuilder: FormBuilder,
                private data: DataService,
                private snackBar: MatSnackBar) {
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
        this.isLoading = false;
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            id: [null],
            name: [null, [Validators.required]],
            secret_key: [null, [Validators.required]],
        });
        this.data.currentProject.subscribe(project => {
            this.project = project;
            this.update();
        });
        this.status = 'Add';
    }
}