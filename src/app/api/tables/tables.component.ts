import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {DataService} from "../../data.service";

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.css']
})


export class TablesComponent implements OnInit {
    baseUrl: string;
    project_id: string;
    api: string;

    update() {
        this.baseUrl = `${this.api}/projects/${this.project_id}`;
    }

    constructor(private data: DataService) {
    }

    ngOnInit() {
        this.data.currentProject.subscribe(project => {
            if (project && project.id) {
                this.project_id = project.id;
                this.update();
            }
        });

        this.data.currentApi.subscribe(next => {
            this.api = next;
            this.update();
        });
    }

}
