export function getAuthHeader(apiKey: string): {
    [header: string]: string | string[];
} {
    return {
        'Authorization': `Bearer ${apiKey}`
    }
}