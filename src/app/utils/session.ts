import {HttpHeaders} from "@angular/common/http";
import {CookieService} from "ng2-cookies"

const cookie = new CookieService();

export function getSession(): string {
    const lsv = cookie.get('session');
    return lsv ? lsv : '';
}

export function setSession(value: string) {
    cookie.set('session', value);
}

export function getHeaders() {
    return new HttpHeaders({
        'Session': getSession()
    });
}