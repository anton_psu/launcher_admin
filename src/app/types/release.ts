interface Release {
    id: number,
    build_id: number,
    build_version: number,
    stage: string
}