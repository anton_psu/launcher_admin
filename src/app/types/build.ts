interface Build {
    id: number
    version: string
    build_number: number
    architecture: string
    file_name: string
    installer_file_name: string
}