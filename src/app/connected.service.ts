import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class ConnectedService {
    private connected = new BehaviorSubject<boolean>(false);
    currentStatus = this.connected.asObservable();

    changeStatus(nextStatus: boolean) {
        this.connected.next(nextStatus);
    }

    constructor() { }
}
