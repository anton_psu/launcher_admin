import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Project} from "./types/project";
import {CookieService} from 'ng2-cookies';

const cookie = new CookieService();

@Injectable()
export class DataService {
    private project = new BehaviorSubject<Project>({
        id: '',
        name: '',
        key: ''
    });
    currentProject = this.project.asObservable();

    changeProject(nextProject: Project) {
        this.project.next(nextProject);
    }

    private api = new BehaviorSubject<string>(cookie.get('api'));
    currentApi = this.api.asObservable();

    changeApi(nextApi: string) {
        cookie.set('api', nextApi);
        this.api.next(nextApi);
    }

    constructor() {
    }

}
