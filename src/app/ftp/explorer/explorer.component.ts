import {Component, EventEmitter, Injectable, OnInit, Output, Pipe, PipeTransform} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from "../../../environments/environment";
import {getSession} from "../../utils/session";

@Pipe({
    name: 'filter'
})
@Injectable()
export class FilterPipe implements PipeTransform {
    transform(itemList: any[], type: string): any[] {
        if (!itemList || !type)
            return itemList;
        return itemList.filter(item => item.type === type);
    }
}

@Component({
    selector: 'app-explorer',
    templateUrl: './explorer.component.html',
    styleUrls: ['./explorer.component.css']
})
export class ExplorerComponent implements OnInit {
    public currentDir: string;
    public items: any[];
    public refreshing: boolean = false;
    @Output() onDisconnect = new EventEmitter<boolean>();

    constructor(private  http: HttpClient,
                public snackBar: MatSnackBar) {
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
    }

    closeConnection() {
        this.http.get(`${environment.api.ftp}/close`, {
            responseType: 'text',
            headers: {
                'Session': getSession()
            }
        }).subscribe(
            (data: any) => {
                if (data === 'OK')
                    this.onDisconnect.emit(false);
                else
                    this.handleError(data.message);
            },
            (err: HttpErrorResponse) => this.handleError(err.message)
        )
    }

    copyToClipboard(item) {
        const url = encodeURIComponent(`https://cdn.xsolla.net/launcher${this.currentDir}${item.name}`);
        const input = document.createElement('input');
        document.body.appendChild(input);
        input.value = url;
        input.select();
        document.execCommand('Copy');
        this.snackBar.open('CDN link has been successfully copied to clipboard', null, {
            duration: 2000
        });
        document.body.removeChild(input);
    }

    list() {
        this.refreshing = true;
        this.http.post(`${environment.api.ftp}/list`, {
            path: this.currentDir
        }, {
            responseType: 'text',
            headers: {
                'Session': getSession()
            }
        }).subscribe(
            (data: any) => {
                this.refreshing = false;
                try {
                    this.items = JSON.parse(data);
                    for (let item of this.items) {
                        if (item.type === '-')
                            item.href = `${environment.api.ftp}/get${this.currentDir}${item.name}?session=${getSession()}`;
                    }
                }
                catch (err) {
                    this.handleError(err.message);
                }
            },
            (err: HttpErrorResponse) => {
                this.refreshing = false;
                this.handleError(err.message);
            }
        )
    }

    get(filepath) {
        let a = document.createElement('a');
        a.href = `${environment.api.ftp}/get${filepath}`;
        a.click();
    }

    onDblClick(item) {
        if (item.type === 'd') {
            if (item.name !== '..')
                this.currentDir += item.name + '/';
            else
                this.currentDir = this.currentDir.split('/').slice(0, -2).join('/') + '/';
            this.list();
        }
        else {
            this.get(`${this.currentDir}${item.name}`);
        }
    }

    ngOnInit() {
        this.currentDir = '/';
        this.list();
    }
}
