import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEventType, HttpHeaders, HttpRequest} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {getSession} from "../../../utils/session";
import {MatSnackBar} from "@angular/material";

@Component({
    selector: 'app-uploader',
    templateUrl: './uploader.component.html',
    styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {
    public isDropping: boolean = false;
    public isLoading: boolean = false;
    public uploadMode = 'determinate';
    public upload: number = 50;
    @Input() currentDir: string;

    constructor(private http: HttpClient,
                public snackBar: MatSnackBar) {
    }

    showMessage(message: string) {
        this.snackBar.open(message, environment.alert.action, environment.alert.info);
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
    }

    onChange(event: any) {
        if (event.target.files && event.target.files[0]) {
            this.uploadFile(event.target.files[0]);
        }
        else {
            this.showMessage('Select only one file');
        }
    }

    uploadFile(file: File) {
        let formData = new FormData();
        const size = file.size;
        this.uploadMode = 'determinate';
        formData.append('file', file);
        formData.append('path', `${this.currentDir}${file.name}`);
        const headers = new HttpHeaders(
            {
                'Session': getSession()
            }
        );
        const req = new HttpRequest('POST', `${environment.api.ftp}/put`, formData, {
            reportProgress: true,
            headers
        });
        this.isLoading = true;
        this.http.request(req).subscribe(
            (event: any) => {
                switch (event.type) {
                    case HttpEventType.Sent:
                        this.upload = 0;
                        break;
                    case HttpEventType.ResponseHeader:
                        this.isLoading = false;
                        this.showMessage('File uploading done!');
                        break;
                    case HttpEventType.UploadProgress:
                        this.upload = Math.floor(event.loaded / size * 100);
                        if (this.upload === 100)
                            this.uploadMode = 'indeterminate';
                        break;
                }
            },
            (err: HttpErrorResponse) => {
                if (err.status === 200) {
                    this.isLoading = false;
                    this.showMessage('File uploading done!');
                }
                else {
                    this.handleError(err.error ? err.error.message : err.message);
                }
            }
        )
    }

    drop(event: any) {
        event.preventDefault();
        event.stopPropagation();
        this.isDropping = false;
        if (event.dataTransfer.files.length !== 1) {
            this.showMessage('Select only one file');
            return;
        }
        if (!this.isLoading) this.uploadFile(event.dataTransfer.files.item(0));
    }

    dragleave(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        this.isDropping = false;
    }

    drag(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        if (!this.isLoading) {
            this.isDropping = true;
        }
    }

    ngOnInit() {
    }

}
