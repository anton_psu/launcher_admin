import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CookieService} from "ng2-cookies";
import {ConnectedService} from "../connected.service";

@Component({
    selector: 'ftp',
    templateUrl: './ftp.component.html',
    styleUrls: ['./ftp.component.css']
})
export class FtpComponent implements OnInit {
    @Output() onConnectEmitter = new EventEmitter<boolean>();
    public cookie = new CookieService();
    public server: string = '';

    updateServer() {
        this.server = this.cookie.get('server')
    }

    onConnect(value: boolean) {
        this.onConnectEmitter.emit(value);
    }

    ngOnInit() {
        this.connectedService.currentStatus.subscribe((next: boolean) => {
            this.updateServer();
        });
    }

    constructor(private connectedService: ConnectedService) {

    }
}
