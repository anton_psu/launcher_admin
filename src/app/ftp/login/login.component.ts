import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

import {getSession, setSession} from '../../utils/session';
import {environment} from '../../../environments/environment';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material";
import {ConnectedService} from "../../connected.service";
import {CookieService} from "ng2-cookies";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    connected: boolean;
    cookies = new CookieService();

    constructor(private http: HttpClient,
                private connectedService: ConnectedService,
                private formBuilder: FormBuilder,
                private snackBar: MatSnackBar,) {
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
    }

    auth(values) {
        this.cookies.set('server', values.server);
        this.try(() => {
            this.http.post(`${environment.api.ftp}/login`, values, {
                responseType: 'text',
                headers: {
                    'Session': getSession()
                }
            }).subscribe(
                (data: any) => {
                    if (data === 'OK')
                        this.connectedService.changeStatus(true);
                    else
                        this.handleError(data.message);
                },
                (err: HttpErrorResponse) => this.handleError(err.error ? err.error : err.message)
            );
        });
    }

    close() {
        this.http.get(`${environment.api.ftp}/close`, {
            responseType: 'text',
            headers: {
                'Session': getSession()
            }
        }).subscribe(
            (data: any) => {
                if (data !== 'OK') this.handleError(data.message)
            },
            (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
        )
    }

    try(onSuccess: Function, onDisconnect: Function = () => void 0) {
        this.http.get(`${environment.api.ftp}/try`, {
            responseType: 'text',
            withCredentials: true,
            headers: {
                'Session': getSession()
            }
        }).subscribe(
            (data: any) => {
                const res = JSON.parse(data);
                this.connectedService.changeStatus(res.connected);
                setSession(res.session);
                onSuccess();
            },
            (err: HttpErrorResponse) => {
                this.handleError(err.message);
                this.connectedService.changeStatus(false);
            }
        );
    }

    ngOnInit() {
        this.connectedService.currentStatus.subscribe((next: boolean) => {
            this.connected = next;
        });
        this.form = this.formBuilder.group({
            server: [this.cookies.get('server') || environment.ftp.server, [Validators.required]],
            login: [environment.ftp.login, [Validators.required]],
            pass: [environment.ftp.pass, [Validators.required]],
        });
        this.try(() => void 0);
        setInterval(() => {
            if (this.connected) this.try(
                    () => void 0
                );
        }, 10000);
    }
}