import {Component, OnInit} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {getSession} from "./utils/session";
import {MatSnackBar} from "@angular/material";
import {ConnectedService} from "./connected.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
    public connected: boolean;
    constructor(private http: HttpClient,
                private connectedService: ConnectedService,
                private snackBar: MatSnackBar) {
    }

    handleError(message) {
        this.snackBar.open(message, environment.alert.action, environment.alert.error);
    }

    onConnect(value) {
        if (this.connected && !value) {
            console.log('close');
            this.http.get(`${environment.api.ftp}/close`, {
                responseType: 'text',
                headers: {
                    'Session': getSession()
                }
            }).subscribe(
                (data: any) => {
                    if (data !== 'OK')
                        this.handleError(data.message);
                },
                (err: HttpErrorResponse) => this.handleError(err.error && JSON.stringify(err.error.error) || err.message)
            );
        }
        this.connectedService.changeStatus(value);
    }

    ngOnInit() {
        this.connectedService.currentStatus.subscribe((next: boolean) => {
            this.connected = next;
        });
    }
}