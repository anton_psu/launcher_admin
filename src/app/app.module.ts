import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatIconRegistry,
    MatInputModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularSplitModule} from 'angular-split';

import {AppComponent} from './app.component';

import {DataService} from './data.service';
import {ConnectedService} from './connected.service';

import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';

import {ApiComponent} from './api/api.component';
import {ProjectComponent} from './api/project/project.component';
import {TablesComponent} from './api/tables/tables.component';
import {StagesComponent} from './api/tables/stages/stages.component';
import {BuildsComponent} from './api/tables/builds/builds.component';
import {ReleasesComponent} from './api/tables/releases/releases.component';

import {FtpComponent} from './ftp/ftp.component';
import {LoginComponent} from './ftp/login/login.component';
import {ExplorerComponent, FilterPipe} from './ftp/explorer/explorer.component';
import {UploaderComponent} from './ftp/explorer/uploader/uploader.component';

@NgModule({
    declarations: [
        AppComponent,

        ConfirmDialogComponent,

        ApiComponent,
        ProjectComponent,
        TablesComponent,
        StagesComponent,
        BuildsComponent,
        ReleasesComponent,

        FtpComponent,
        LoginComponent,
        ExplorerComponent,
        UploaderComponent,
        FilterPipe
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'my-app'}),
        BrowserAnimationsModule,
        HttpClientModule,
        MatSelectModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        MatTableModule,
        FormsModule,
        MatDialogModule,
        MatCardModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        AngularSplitModule,
    ],
    providers: [
        DataService,
        ConnectedService
    ],
    entryComponents: [ConfirmDialogComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
        matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
    }
}
