import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as fs from 'fs';
import * as path from 'path';
import * as Client from 'ftp';
import * as multer from 'multer';
import * as uuid from 'uuid/v4';
import * as https from 'https';

const config: any = JSON.parse(fs.readFileSync('./config.server.json', 'utf8'));

const useHttps = fs.existsSync(`${config.credentials}privatekey.key`)
    && fs.existsSync(`${config.credentials}certificate.crt`);

const PORT: number = +process.env.PORT || config.defaultPort;

const upload: any = multer({dest: config.tmpDir});

const app: express.Application = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
if (!config.production) {
    const cors = require('cors');
    app.use(cors({
        origin: ['http://localhost:4200', 'http://localhost:4000'],
        credentials: true
    }));
}
const connections: any = {};

function createConnection(object: any, uuid: string) {
    const c = new Client();
    object.connection = c;
    object.res = null;
    object.proj = false;
    c.on('error', (err: any) => {
        console.log('FTP-ERROR');
        console.log(err.message);
        try {
            object.proj = false;
            object.res.status(599).send(err.message);
        }
        catch (err) {
        }
    });
    c.on('ready', () => {
        object.res.sendStatus(200);
    });
    setTimeout(() => {
        object.connection.destroy();
        delete connections[uuid];
    }, config.lifetime);

    setTimeout(() => {
        object.connection.destroy();
    }, config.connectionTime);
}

const proj: express.Router = express.Router();
proj.route('/project').get((req: express.Request, res: express.Response) => {
    if (req.headers.session
        && connections[<string>req.headers.session]
        && connections[<string>req.headers.session].proj) {

        if (!fs.existsSync(config.projects)) {
            fs.mkdirSync(config.projects);
        }

        const filepath = `${config.projects}${path.basename(connections[<string>req.headers.session].proj)}`;
        if (fs.existsSync(filepath)) {
            const data = fs.readFileSync(filepath);
            res.send(data);
        }
        else {
            fs.writeFile(filepath, '[]', function (err) {
                if (err)
                    return console.log(err);
            });
            res.send(JSON.stringify([]));
        }
    }
    else {
        res.sendStatus(401);
    }
});

proj.route('/project').post((req: express.Request, res: express.Response) => {
    if (!<string>req.headers.session || !connections[<string>req.headers.session] || !connections[<string>req.headers.session].proj) {
        res.sendStatus(401);
        return;
    }

    const filepath = `${config.projects}${path.basename(connections[<string>req.headers.session].proj)}`;
    if (fs.existsSync(filepath)) {
        fs.writeFileSync(filepath, JSON.stringify(req.body));
        res.send(req.body);
    }
    else {
        res.sendStatus(404);
    }
});

proj.route('/project/:name').patch((req: express.Request, res: express.Response) => {
    if (!<string>req.headers.session || !connections[<string>req.headers.session] || !connections[<string>req.headers.session].proj) {
        res.sendStatus(401);
        return;
    }

    const filepath = `${config.projects}${path.basename(connections[<string>req.headers.session].proj)}`;
    if (fs.existsSync(filepath)) {
        fs.writeFileSync(filepath, JSON.stringify(req.body));
        res.sendStatus(200);
    }
    else {
        res.sendStatus(404);
    }
});

const get = express.Router();

get.route('/try').get((req: express.Request, res: express.Response) => {
    const connection = connections[<string>req.headers.session];
    if (<string>req.headers.session && connection) {
        res.send({
            connected: connection.connection.connected,
            session: req.headers.session
        });
    }
    else {
        const instance = uuid();
        createConnection(connections[instance] = {}, instance);
        res.send({
            session: instance,
            connected: false
        });
    }
});

get.route('/login').post((req: express.Request, res: express.Response) => {
    if (!<string>req.headers.session || !connections[<string>req.headers.session]) {
        res.sendStatus(401);
        return;
    }
    const connection = connections[<string>req.headers.session];

    const hostRE = /(.+):(\d+)/;
    const hostData = req.body.server.match(hostRE);
    let options;
    if (!!hostData) {
        options = {
            user: req.body.login,
            password: req.body.pass,
            host: hostData[1],
            port: hostData[2]
        };
    }
    else {
        options = {
            user: req.body.login,
            password: req.body.pass,
            host: req.body.server
        };
    }

    connection.res = res;
    connection.proj = req.body.login || 'anonymous';
    connection.connection.connect(options);
});

get.route('/close').get((req: express.Request, res: express.Response) => {
    if (!<string>req.headers.session || !connections[<string>req.headers.session]) {
        res.sendStatus(401);
        return;
    }
    connections[<string>req.headers.session].connection.destroy();
    res.sendStatus(200);
});

get.route('/list').post((req: express.Request, res: express.Response) => {
    if (!<string>req.headers.session || !connections[<string>req.headers.session]) {
        res.sendStatus(401);
        return;
    }
    const {path} = req.body;
    connections[<string>req.headers.session].res = res;
    connections[<string>req.headers.session].connection.list(path, (err, list) => {
        if (err)
            res.send({code: err.code, message: err.message});
        else {
            res.status(200).send(list);
        }
    })
});

get.route('/get/:filepath').get((req: express.Request, res: express.Response) => {
    const {filepath} = req.params;
    const {session} = req.query;
    connections[session].res = res;
    connections[session].connection.list(filepath, (err, list) => {
        connections[session].connection.get(filepath, (err, filestream) => {
            if (err)
                res.send({code: err.code, message: err.message});
            else {
                res.set('Content-Length', list[0].size);
                filestream.pipe(res);
            }
        })
    });

});

get.route('/put').post(upload.single('file'), (req: any, res: express.Response) => {
    if (!<string>req.headers.session || !connections[<string>req.headers.session]) {
        res.sendStatus(401);
        return;
    }
    const {path} = req.body;
    connections[<string>req.headers.session].connection.put(req.file.path, path, (err) => {
        if (err) {
            res.status(err.code).send({code: err.code, message: err.message});
            return;
        }
        res.sendStatus(200);
        fs.unlink(req.file.path, err => console.log(err));
    });
});

const ftpRoute: express.Router = express.Router();
ftpRoute.use('/ftp', get);
app.use('/api', ftpRoute);

const projRoute: express.Router = express.Router();
projRoute.use('/proj', proj);
app.use('/api', projRoute);

app.use(express.static('./dist/browser'));

let server;

if (useHttps) {
    const privateKey = fs.readFileSync(`${config.credentials}privatekey.key`, 'utf8');
    const certificate = fs.readFileSync(`${config.credentials}certificate.crt`, 'utf8');
    const credentials = {key: privateKey, cert: certificate};
    server = https.createServer(credentials, app);
}
else {
    server = app;
}

server.listen(PORT, () => {
    console.log(`Node Express server listening on http${useHttps ? 's' : ''}://localhost:${PORT}`);
});