# Launcher releases admin
Web interface for launcher releases

Requires node 8+ version

## Install
`npm install` or `yarn`

## Build
Setup configuration:
1. Edit in `/src/enviroments/enviroment.prod.ts` fields that listed below only
    ```js
        // default valuest for login
        ftp: {
            server: 'us-static.loc',
            login: '',
            pass: ''
        }
    ```
2. Edit `config.server.json`:
    ```js
    {
        "production": true, 
        "lifetime": 1200000, // session lifetime in ms
        "connectionTime": 1200000, // ftp session lifetime in ms
        "defaultPort": 4000, // node server port 
        "tmpDir": "./tmp/", // folder with temporary files
        "credentials": "./credentials/", // path to ssl keys pair
        "projects": "./projects/" // stores projects information: api key, id, name
    }
    ```

and then run 
`npm run build` or `yarn run build`

## Running
`npm run start` or `yarn run start`

For https put SSL certificate file named `certificate.crt` and SSL certificate key file named `privatekey.key` in the directory you setup above ("credentials" parameter).